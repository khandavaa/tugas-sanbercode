<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ReviewController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class,'index']);
Route::get('/register', [AuthController::class,'register']);
Route::post('/kirim', [AuthController::class,'kirim']);

Route::get('/data-table', function(){
    return view('page.datatable');
});

//crud

Route::group(['middleware' => ['auth']], function () {
    //Create Data
//menuju ke form inputan tambah data
//create for cast
Route::get('/cast/create',[CastController::class,'create']);
Route::get('/film/create',[FilmController::class,'create']);
Route::get('/genre/create',[GenreController::class,'create']);
//create post data
Route::post('/cast',[CastController::class,'store']);
Route::post('/film',[FilmController::class,'store']);
Route::post('/genre',[GenreController::class,'store']);

//Read Data
//route untuk tampil semua data "show
Route::get('/cast',[CastController::class,'index']);
Route::get('/film',[FilmController::class,'index']);
Route::get('/genre',[GenreController::class,'index']);

//route dinamis yang bisa get data dari detail berdasarkan id
Route::get('/cast/{id}',[CastController::class,'show']);
Route::get('/film/{id}',[FilmController::class,'show']);
Route::get('/genre/{id}',[GenreController::class,'show']);


//update data
//menuju ke form inputan tambah data berdasarkan id
Route::get('/cast/{id}/edit',[CastController::class,'edit']);
Route::get('/film/{id}/edit',[FilmController::class,'edit']);
Route::get('/genre/{id}/edit',[GenreController::class,'edit']);
//update data di databse berdasarkan id

Route::put('/cast/{id}',[CastController::class,'update']);
Route::put('/film/{id}',[FilmController::class,'update']);
Route::put('/genre/{id}',[GenreController::class,'update']);


//detele data
Route::delete('/cast/{id}',[CastController::class,'destroy']);
Route::delete('/film/{id}',[FilmController::class,'destroy']);
Route::delete('/genre/{id}',[GenreController::class,'destroy']);

//route review
Route::post('/review/{id}',[ReviewController::class,'komen']);


});



//buat crud post
Route::resource('/film', FilmController::class);

Auth::routes();


