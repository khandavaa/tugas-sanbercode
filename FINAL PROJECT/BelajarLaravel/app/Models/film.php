<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    use HasFactory;
    protected $table ='film';
    protected $fillable =['judul','ringkasan','tahun','poster','genre_id',];
    
    public function filemw()
    {
        return $this->belongsTo(genre::class, 'genre_id');
    }

    public function komen()
    {
        return $this->hasMany(review::class, 'film');
    }
}
