<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    use HasFactory;
    protected $table ='genre';
    protected $fillable =['id','nama'];
    
    public function genrew()
    {
        return $this->hasMany(film::class, 'genre_id');
    }
}
