<?php

namespace App\Http\Controllers;
use App\Models\genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except(['index', 'show']);
    }
    public function create()
    {
        $genre = genre::get();
        return view('genre.formgenre',['genre'=> $genre]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255',
            
            // 'genre' => 'required|integer',
            // 'judul' => 'required|unique:posts|max:255',
            // 'ringkasan' => 'required',
            // 'tahun' => 'required',
            // 'poster' => 'required',

        ]);
        // $filename =time().'.'.$request->poster->extension();
        // $request->file->move(public_path('upload'),$filename);

        genre::create([
            'nama'=> $request['nama'],
            // 'ringkasan'=> $request['ringkasan'],
            // 'tahun'=> $request['tahun'],
            // 'poster'=> $request['poster'],
            // // 'genre'=> $request['genre'],
        ]);
        
        // genre::create([
        //     'judul'=> $request['judul'],
        //     'ringkasan'=> $request['ringkasan'],
        //     'tahun'=> $request['tahun'],
        //     'poster'=> $request['poster'],

        // ]);

        return redirect('/genre');
    }

    public function index(){
        $genreeries = genre::all();
        // dd($genreeries);
        return view('genre.showgenre', ['genreeries' => $genreeries]);
    }

    public function show($id){
        $genre = genre::where('id', $id)->first();
        return view('genre.detailgenre', ['genre' => $genre]);

    }

    public function edit($id){
        $genre = genre::where('id', $id)->first();
        return view('genre.editgenre', ['genre' => $genre]);

    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|',
            // 'ringkasan' => 'required',
            // 'tahun' => 'required|integer',
            // 'poster' => 'required|image|mimes:jpg,png,jpeg',
            // 'genre' => 'required|integer',
            // 'judul' => 'required|unique:posts|max:255',
            // 'ringkasan' => 'required',
            // 'tahun' => 'required',
            // 'poster' => 'required',
        ]);

        $genre = genre::find($id);
        $genre->nama = $request['nama'];
        // $genre->ringasan = $request['ringkasan'];
        // $genre->tahun = $request['tahun'];
        // // $genre->genre = $request['genre'];
        // $genre->poster = $request['poster'];

        $genre->save();
        return redirect('/genre');

    }

    public function destroy($id){
        $genre = genre::find($id);
        $genre->delete();
        return redirect('/genre');
    }
}
