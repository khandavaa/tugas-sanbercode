<?php

namespace App\Http\Controllers;
use App\Models\film;
use App\Models\genre;
use Illuminate\Http\Request;
use File;
class FilmController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except(['index', 'show']);
    }
    public function create()
    {
        $genre = genre::get();
        return view('film.formfilm',['genre'=> $genre]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'ringkasan' => 'required',
            'tahun' => 'required|integer',
            'poster' => 'required|image|mimes:jpg,png,jpeg',
            'genre_id' => 'required|integer',
            // 'judul' => 'required|unique:posts|max:255',
            // 'ringkasan' => 'required',
            // 'tahun' => 'required',
            // 'poster' => 'required',

        ]);
        $filename =time().'.'.$request->poster->extension();
        $request->poster->move(public_path('image'), $filename);
        $film = new film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $filename;
        $film->genre_id = $request->genre_id;
        $film->save();


        // film::create([
        //     'judul'=> $request['judul'],
        //     'ringkasan'=> $request['ringkasan'],
        //     'tahun'=> $request['tahun'],
        //     'poster'=> $request['poster'],
        //     'genre_id'=> $request['genre_id'],
        // ]);
        
        // Film::create([
        //     'judul'=> $request['judul'],
        //     'ringkasan'=> $request['ringkasan'],
        //     'tahun'=> $request['tahun'],
        //     'poster'=> $request['poster'],

        // ]);

        return redirect('/film');
    }

    public function index(){
        $filmeries = film::all();
        // dd($filmeries);
        return view('film.showfilm', ['filmeries' => $filmeries]);
    }

    public function show($id){
        $film = film::where('id', $id)->first();
        return view('film.detailfilm', ['film' => $film]);

    }

    public function edit($id){
         $film = film::find($id);
         $genre = genre::get();

         return view('film.editfilm', ['film' => $film, 'genre'=> $genre]);
        // $film = film::where('id', $id)->first();
        // return view('film.editfilm', ['film' => $film]);

    }

    public function update(Request $request, $id){
        $request->validate([
            'judul' => 'required|max:255',
            'ringkasan' => 'required',
            'tahun' => 'required|integer',
            'poster' => '|image|mimes:jpg,png,jpeg',
            'genre_id' => 'required|integer',
            // 'judul' => 'required|unique:posts|max:255',
            // 'ringkasan' => 'required',
            // 'tahun' => 'required',
            // 'poster' => 'required',
        ]);

        $film = film::find($id);
        if($request->has('poster')){
            $path = 'image/';
            file::delete($path. $film->poster);
            $filename =time().'.'.$request->poster->extension();
            $request->poster->move(public_path('image'), $filename);
            $film->poster= $filename;
            $film->save();
        }


        $film->judul = $request['judul'];
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->genre_id = $request['genre_id'];

        $film->save();
        return redirect('/film');

    }

    public function destroy($id){
        $film = film::find($id);

        $path = 'image/';
        file::delete($path. $film->poster);
        $film->delete();
        return redirect('/film');
    }
}

