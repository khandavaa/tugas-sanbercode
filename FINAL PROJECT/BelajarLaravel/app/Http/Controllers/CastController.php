<?php

namespace App\Http\Controllers;
use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.formcast');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required',
            'bio' => 'required',
            // 'judul' => 'required|unique:posts|max:255',
            // 'ringkasan' => 'required',
            // 'tahun' => 'required',
            // 'poster' => 'required',

        ]);
        Cast::create([
            'nama'=> $request['nama'],
            'umur'=> $request['umur'],
            'bio'=> $request['bio'],
        ]);
        // Film::create([
        //     'judul'=> $request['judul'],
        //     'ringkasan'=> $request['ringkasan'],
        //     'tahun'=> $request['tahun'],
        //     'poster'=> $request['poster'],

        // ]);

        return redirect('/cast');
    }

    public function index(){
        $casteries = cast::all();
        // dd($casteries);
        return view('cast.showcast', ['casteries' => $casteries]);
    }

    public function show($id){
        $cast = cast::where('id', $id)->first();
        return view('cast.detailcast', ['cast' => $cast]);

    }

    public function edit($id){
        $cast = cast::where('id', $id)->first();
        return view('cast.editcast', ['cast' => $cast]);

    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required',
            'bio' => 'required',
            // 'judul' => 'required|unique:posts|max:255',
            // 'ringkasan' => 'required',
            // 'tahun' => 'required',
            // 'poster' => 'required',
        ]);

        $cast = cast::find($id);
        $cast->nama = $request['nama'];
        $cast->umur = $request['umur'];
        $cast->bio = $request['bio'];

        $cast->save();
        return redirect('/cast');

    }

    public function destroy($id){
        $cast = cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}