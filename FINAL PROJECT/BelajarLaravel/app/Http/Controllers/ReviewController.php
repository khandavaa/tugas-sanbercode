<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\review;
use Illuminate\Support\Facades\Auth;


class ReviewController extends Controller
{
    public function komen($id, Request $request)
    {
        $request->validate([
            'content' => 'required',
            'poin' =>'required',
        ]);
        $iduser = Auth::id();
        $Review = new review;
        $Review->content = $request['content'];
        $Review->poin = $request['poin'];

        $Review->film =$id;
        $Review->users =$iduser;
        $Review->save();

        return redirect('/film/'. $id);
    }
}
