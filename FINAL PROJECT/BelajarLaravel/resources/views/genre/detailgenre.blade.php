@extends('layout.master')
@section('title')
    Page Detail Genre
@endsection
@section('content')

<h1>{{$genre->id}}</h1>
<p>{{$genre->nama}}</p>
<div class="row">
   @forelse ($genre->genrew as $item) 
   <div class="col-4">
    <div class="card">
    <img src="{{asset('image/'. $item->poster)}}" class="img-fluid img-thumbnail" alt="...">
         <div class="card-body">
            <h3 class="card-title">{{$item->judul}}</h3>
            <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
            <p>{{$genre->nama}}</p>
                    
         </div>
    </div>
    </div>
@empty
    <h2>Tidak ada Data Film Disini</h2>
@endforelse
</div>
@endsection