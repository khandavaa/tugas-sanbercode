@extends('layout.master')
@section('title')
    Page Data Genre
@endsection
@section('content')
<a href="http:/genre/create" class="btn btn-primary btn-sm mb-4">Tambah Genre</a>

<table class="table table-bordered table-dark">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>

      </tr>
    </thead>
    <tbody>
        @forelse ($genreeries as $key => $item)
        <tr>
            <th scope="row">{{$key +1}}</th>
            <td>{{$item-> nama}}</td>
            <td>
                <form action="/genre/{{$item->id}}" method="post"> 
                @csrf
                @method('delete')
                <a href="/genre/{{$item->id}}" class="btn btn-sm btn-info">Detail Genre</a>
                <a href="/genre/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a> 
                <input type="submit" onclick="return confirm('Apakah Ingin Dihapus?')" value="delete" class="btn btn-sm btn-danger">
                </form>

            </td>
          </tr>
          
        @empty
            <h1>Nothing Data in Here</h1>
        @endforelse

    </tbody>
  </table>
  @endsection