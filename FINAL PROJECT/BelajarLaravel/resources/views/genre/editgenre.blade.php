@extends('layout.master')
@section('title')
    Edit Film Profile
@endsection
@section('content')

<form action="/genre/{{$genre->id}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('put')
{{-- //table film --}}
    <div class="form-group">
    <label >Nama</label>
      <input type="text" value="{{$genre->nama}}" name="nama"class="form-control">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection