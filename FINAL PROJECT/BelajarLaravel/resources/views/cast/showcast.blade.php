@extends('layout.master')
@section('title')
    Page View Actor
@endsection
@section('content')

 <button type="submit" class="btn btn-info btn-block">Search</button>
<table class="table table-bordered table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casteries as $key => $item)
        <tr>
            <th scope="row">{{$key +1}}</th>
            <td>{{$item-> nama}}</td>
            <td>{{$item-> umur}}</td>
            <td>{{$item-> bio}}</td>
            <td>
                
                <form action="/cast/{{$item->id}}" method="post"> 
                @csrf
                @method('delete')
                <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail Actor</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a> 
                <input type="submit" onclick="return confirm('Apakah Ingin Dihapus?')" value="delete" class="btn btn-sm btn-danger">
                </form>

            </td>
          </tr>
          
        @empty
            <h1>Nothing Data in Here</h1>
        @endforelse

    </tbody>
  </table>
  @endsection