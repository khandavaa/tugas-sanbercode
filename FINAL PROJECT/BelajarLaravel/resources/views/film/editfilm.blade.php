@extends('layout.master')
@section('title')
    Edit Film Profile
@endsection
@section('content')

<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('put')
{{-- //table film --}}
    <div class="form-group">
    <label >Judul</label>
      <input type="text" value="{{$film->judul}}" name="judul"class="form-control">
    </div>
    @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >Ringkasan</label>
        <input type="text" value="{{$film->ringkasan}}" name="ringkasan"class="form-control">
      </div>
      @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
        <label >Tahun</label>
          <input type="integer" value="{{$film->tahun}}" name="tahun"class="form-control">
        </div>
        @error('tahun')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
            <label >Poster</label>
            <input type="file" value="{{$film->poster}}"  name="poster"class="form-control">
            </div>
            @error('poster')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label >Genre</label>
            <select name="genre_id" class="form-control" id="">
              <option value="">-- Pilih Genre --</option>
              @forelse ($genre as $item)
                @if ($item->id === $film->genre_id)
                  <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                  <option value="{{$item->id}}">{{$item->nama}}</option>
                  @endif
                @empty
                <option value="">Tidak ada datanya cuy</option>
              @endforelse
            </select>
            
            </div>
          
    {{-- //end table film --}}

    {{-- //table film --}}
    {{-- <div class="form-group">
      <label >Judul</label>
      <input type="string" name="judul"class="form-control">
      </div>

    <div class="form-group">
            <label >Ringkasan</label>
            <input type="text" name="ringkasan"class="form-control">
            </div>

    <div class="form-group">
            <label >Tahun</label>
            <input type="integer" name="tahun"class="form-control">
            </div>

    <div class="form-group">
            <label >poster</label>
            <input type="string" name="poster"class="form-control">
            </div> --}}
{{-- //end table Film --}}

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection