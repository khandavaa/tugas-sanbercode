@extends('layout.master')
@section('title')
    Form Page Film
@endsection
@section('content')

<form action="/film" method="POST" enctype="multipart/form-data">
  @csrf
{{-- //table film --}}
    <div class="form-group">
    <label >Judul</label>
      <input type="text" name="judul"class="form-control">
    </div>
    @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >Ringkasan</label>
        <input type="text" name="ringkasan"class="form-control">
      </div>
      @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
        <label >Tahun</label>
          <input type="integer" name="tahun"class="form-control">
        </div>
        @error('tahun')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

    <div class="form-group">
            <label >Poster</label>
            <input type="file" name="poster"class="form-control">
            </div>
            @error('poster')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label >Genre</label>
            <select name="genre_id" class="form-control" id="">
              <option value="">-- Pilih Genre --</option>
                @forelse ($genre as $item)
                    <option value="{{$item->id}}"> {{$item->nama}} </option>
                @empty
                    <option value="">Tidak ada datanya cuy</option>
                @endforelse
            </select>
            @error('genre_id')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror


        {{-- <div class="form-group">
            <label >genre</label>
            <input type="integer" name="genre"class="form-control">
            </div>
            @error('genre')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}
    {{-- //end table film

    {{-- //table film --}}
    {{-- <div class="form-group">
      <label >Judul</label>
      <input type="string" name="judul"class="form-control">
      </div>

    <div class="form-group">
            <label >Ringkasan</label>
            <input type="text" name="ringkasan"class="form-control">
            </div>

    <div class="form-group">
            <label >Tahun</label>
            <input type="integer" name="tahun"class="form-control">
            </div>

    <div class="form-group">
            <label >poster</label>
            <input type="string" name="poster"class="form-control">
            </div> --}}
{{-- //end table Film --}}

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection