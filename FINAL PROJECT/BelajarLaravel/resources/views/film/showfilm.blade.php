@extends('layout.master')
@section('title')
    Page Data Film
@endsection
@section('content')
<marquee title="Ini Muncul Saat Hover" behavior="alternate" onmouseover="this.stop()" onmouseout="this.start()" direction="right"><a href="/film" target="_blank">WATCH YOUR FAVORITE MOVIE!</a></marquee>
@auth
   <a href="http:/film/create" class="btn btn-primary btn-sm mb-4">Tambah Post</a> 
@endauth

<div class="row">
@forelse ($filmeries as $item)

<div class="col- 4">

    <div class="card">
      <a href="film/{{$item->id}}">
      <img src="{{asset('image/'. $item->poster)}}" style="height: 300px" class="img-fluid img-thumbnail" alt="...">
      </a>
        <div class="card-body">
          <h5 class="card-title">{{$item->judul}}</h5><br>
          <span class="badge badge-secondary">{{$item->filemw->nama}}</span>
          <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
          <a href="/film/{{$item->id}}" class="btn btn-primary btn-block btn-sm">Read Me</a>
           <div class="row my-2">
            <div class="col">
              @auth
                  <a href="/film/{{$item->id}}/edit" class="btn btn-sm btn-warning btn-block">Edit</a>
              @endauth
            </div> 
            <div class="col">
              @auth
                  <form action="film/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                </form>
              @endauth
                
          </div>
        </div>

        </div>
      </div>
</div>
@empty
    <h2>Tidak ada Film</h2>
@endforelse

 {{-- <button type="submit" class="btn btn-info btn-block">Search</button>
<table class="table table-bordered table-dark">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">judul</th>
        <th scope="col">tahun</th>
        <th scope="col">poster</th>
        <th scope="col">genre</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($filmeries as $key => $item)
        <tr>
            <th scope="row">{{$key +1}}</th>
            <td>{{$item-> judul}}</td>
            <td>{{$item-> tahun}}</td>
            <td>{{$item-> poster}}</td>
            <td>{{$item-> genre_id}}</td>
            <td>
                
                <form action="/film/{{$item->id}}" method="post"> 
                @csrf
                @method('delete')
                <a href="/film/{{$item->id}}" class="btn btn-sm btn-info">Detail Actor</a>
                <a href="/film/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a> 
                <input type="submit" onclick="return confirm('Apakah Ingin Dihapus?')" value="delete" class="btn btn-sm btn-danger">
                </form>

            </td>
          </tr>
          
        @empty
            <h1>Nothing Data in Here</h1>
        @endforelse

    </tbody>
  </table> --}}
  @endsection