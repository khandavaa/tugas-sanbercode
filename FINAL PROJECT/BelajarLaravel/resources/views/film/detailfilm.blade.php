@extends('layout.master')
@section('title')
    Page Detail Film
@endsection
@section('content')

    <img src="{{asset('image/'. $film->poster)}}" class="img-thumbnail" style="height: 300px" alt="...">
    
      <h3>{{$film->judul}}</h3>
      <p class="card-text">{{$film->ringkasan}}</p>
      <h1>{{$film->genre_id}}</h3>

      <hr>
        <h3>List Review</h3>
        @forelse ($film->komen as $item)
        <div class="card">
          <div class="card-header">
            {{$item->user->name}}
          </div>
          <div class="card-body">
            <p class="card-text">{{$item->content}}</p>
            <p class="card-text">{{$item->poin}}</p>
          </div>
        </div>
        @empty
            <h1> Belum Ada Komentar </h1>
        @endforelse
       <hr>
        <form action="/review/{{$film->id}}" class="my-5" method="post">
        @csrf
        <textarea name="content" id="" class="form-control my-2" cols="30" rows="10" placeholder="Isi Review"></textarea>
        @error('content')
          <div class="alert alert-danger" role="alert">
          {{$message}}
          </div>
        @enderror
        <div class="form-group">
          <label >Poin</label>
            <input type="integer" name="poin"class="form-control" placeholder="1-5">
          </div>
          @error('poin')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <input type="submit" value="kirim" class="btn btn-primary btn-sm my-2">
        </form>

      <hr>

      <a href="/film" class="btn btn-danger btn-block btn-sm">Back</a>
    
{{-- <h1>{{$film->judul}}</h1>
<p>{{$film->ringkasan}}</p>
<p>{{$film->tahun}}</p>
<p>{{$film->poster}}</p>
<p>{{$film->genre_id}}</p> --}}
</div>

@endsection