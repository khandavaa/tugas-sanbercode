 <!-- Sidebar -->
 <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('/template/dist/img/user2-160x160.jpg')}}" class= "img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        @auth
        <a href="#" class="d-block">  {{ Auth::user()->name }} </a>
      @endauth
      
      @guest
 <a href="/login" class="d-block"> Belom Login </a>
      @endguest
        </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
             <li class="nav-item">
                <a href="/" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    Halaman
                    <span class="right badge badge-danger">New</span>
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                  <p>
                    Cast Page
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/cast" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Cast View</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/cast/create" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Create Cast</p>
                    </a>
                  </li>
                </ul>
                  
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Data Table
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/data-table" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>halaman</p>
              </a>
            </li>
        
          </ul>
          
        </li>
            <li class="nav-item">
          <a href="/film" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Film
              <span class="badge badge-danger">Hot!</span>
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/film" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Film</p>
                <span class="right badge badge-danger">HOT!</span>
              </a>
              <a href="/film/create" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Create Film</p>
              </a>
             </li>   
          </ul>     
        @auth
        <li class="nav-item">
          <a href="/genre" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Genre
              <span class="badge badge-success">New!</span>
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/genre" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Genre</p>
                <span class="badge badge-success">New</span>
              </a>
              <a href="/genre/create" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Create New Genre</p>
              </a>
            </li>
        
          </ul>
          
        </li>
        @endauth

        
    
    </nav>
    <!-- /.sidebar-menu -->
    {{-- logout --}}
    {{-- <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
      {{ Auth::user()->name }}
  </a>

  <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
      <a class="dropdown-item" href="{{ route('logout') }}"
         onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
          {{ __('Logout') }}
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
          @csrf
      </form>
  </div> --}}

  {{-- ini buat logout --}}
  @auth
       <li class="nav-item">
    <a href="{{ route('logout') }}"
    onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();" class="nav-link">
    
      <p>
        Logout
        <span class="right badge badge-danger">!</span>
      </p>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
          @csrf
      </form>
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
      @csrf
  </form>
  </li>
  @endauth
{{-- end logout --}}
 

{{-- ini buat login --}}
@guest
      <li class="nav-item bg-primary">
  <a href="/login" class="nav-link">
    
    <p>
      Login
      <span class="right badge badge-primary">Disini</span>
    </p>
  </a>
</li>
@endguest
  

{{-- ini end login --}}
  </div>
  <!-- /.sidebar -->