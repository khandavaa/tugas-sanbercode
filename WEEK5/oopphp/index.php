<?php
require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("shaun");


echo "Nama = " . $sheep->name ."<br>";
echo "Jumlah Kaki = " . $sheep->legs ."<br>";
echo "Cold Blooded = " . $sheep->cold_blooded ."<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Nama = " . $kodok->name ."<br>";
echo "Jumlah Kaki = " . $kodok->legs ."<br>";
echo "Cold Blooded = " . $kodok->cold_blooded ."<br>";
$kodok->Jump() ."<br>";
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Nama = " . $sungokong->name ."<br>";
echo "Jumlah Kaki = " . $sungokong->legs ."<br>";
echo "Cold Blooded = " . $sungokong->cold_blooded ."<br>";
$sungokong->yell()."<br>";
echo "<br>";


?>