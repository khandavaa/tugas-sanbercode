@extends('layout.master')
@section('title')
    Create New Film Profile
@endsection
@section('content')

<form action="/cast" method="POST">
  @csrf
{{-- //table cast --}}
    <div class="form-group">
    <label > Nama Pemain Film Baru</label>
      <input type="text" name="nama"class="form-control">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Umur</label>
          <input type="integer" name="umur"class="form-control">
        </div>
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
            <label >Bio</label>
            <input type="text" name="bio"class="form-control">
            </div>
            @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    {{-- //end table cast --}}

    {{-- //table film --}}
    {{-- <div class="form-group">
      <label >Judul</label>
      <input type="string" name="judul"class="form-control">
      </div>

    <div class="form-group">
            <label >Ringkasan</label>
            <input type="text" name="ringkasan"class="form-control">
            </div>

    <div class="form-group">
            <label >Tahun</label>
            <input type="integer" name="tahun"class="form-control">
            </div>

    <div class="form-group">
            <label >poster</label>
            <input type="string" name="poster"class="form-control">
            </div> --}}
{{-- //end table Film --}}

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection