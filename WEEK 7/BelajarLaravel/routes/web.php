<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class,'index']);
Route::get('/register', [AuthController::class,'register']);
Route::post('/kirim', [AuthController::class,'kirim']);

Route::get('/data-table', function(){
    return view('page.datatable');
});

//crud
//Create Data
//menuju ke form inputan tambah data
//create for cast
Route::get('/cast/create',[CastController::class,'create']);
//create post data
Route::post('/cast',[CastController::class,'store']);

//Read Data
//route untuk tampil semua data "show
Route::get('/cast',[CastController::class,'index']);

//route dinamis yang bisa get data dari detail berdasarkan id
Route::get('/cast/{id}',[CastController::class,'show']);


//update data
//menuju ke form inputan tambah data berdasarkan id
Route::get('/cast/{id}/edit',[CastController::class,'edit']);
//update data di databse berdasarkan id

Route::put('/cast/{id}',[CastController::class,'update']);


//detele data
Route::delete('/cast/{id}',[CastController::class,'destroy']);


